package in.srishti.musicalapp;

import java.util.ArrayList;

/**
 * Created by Srishti.Gupta on 26-01-2018.
 */

class Song {
    private int mId; // id of the song as in XML
    private String mTitle; // title of the song
    private ArrayList<String> mArtists; // artist(s) of the song

    // constructor
    Song(String title, ArrayList<String> singersList) {
        this.mTitle = title;
        this.mArtists = singersList;
    }

    /**
     * Method to get the id of a song
     * @return id of the song in XML
     */
    public int getId() {
        return mId;
    }

    /**
     * Method to get a song's title
     * @return title of the song
     */
    public String getTitle() {
        return mTitle;
    }

    /**
     * Method to get a song's artists
     * @return artists of the song
     */
    public ArrayList<String> getArtists() {
        return mArtists;
    }
}