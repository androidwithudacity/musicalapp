package in.srishti.musicalapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SearchActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        // Displaying description text for the activity in the TextView
        TextView activityDescriptionTextView = findViewById(R.id.search_song_description_textview);
        String descriptionText = "This screen allows one to search for a song.";
        activityDescriptionTextView.setText(descriptionText);

        // Setting OnClickListener for the buttons in the activity
        Button viewAllBtn = findViewById(R.id.home_btn);
        viewAllBtn.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent listIntent = new Intent(SearchActivity.this, ListActivity.class);
                startActivity(listIntent);
            }
        });
    }
}