package in.srishti.musicalapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        // Displaying description text for the activity in the TextView
        TextView activityDescriptionTextView = findViewById(R.id.song_details_description_textview);
        String descriptionText = "This screen displays the details of the song that is currently being played.";
        activityDescriptionTextView.setText(descriptionText);

        // Get information stored in bundle of the intent
        Bundle songInfoBundle = getIntent().getExtras();
        String title = songInfoBundle.getString(getString(R.string.title));
        ArrayList<String> artistsArrList = songInfoBundle.getStringArrayList(getString(R.string.artist));

        // Display information in the TextViews in the XML
        TextView titleTextView = findViewById(R.id.song_details_title);
        titleTextView.setText(getString(R.string.title) + ": " + title); // display title of the song

        TextView artistsTextView = findViewById(R.id.song_details_artists);
        String artistsStr = "";
        for (String artist: artistsArrList) {
            artistsStr += artist + ", ";
        }
        artistsStr = artistsStr.trim(); // trimming off trailing blank space
        artistsStr = artistsStr.substring(0, artistsStr.length() - 1); // trimming off extra comma
        String artistPlainText = getString(R.string.artist);
        if (artistsArrList.size() > 1) // plural form of artist viz. artists, if the number of artists is more than 1
            artistPlainText = getString(R.string.artist);
        artistsTextView.setText(artistPlainText + ": " + artistsStr);

        // Setting OnClickListener for the buttons in the activity
        Button buyBtn = findViewById(R.id.buy_btn);
        buyBtn.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent paymentIntent = new Intent(DetailsActivity.this, PaymentActivity.class);
                startActivity(paymentIntent);
            }
        });

        Button viewAllBtn = findViewById(R.id.home_btn);
        viewAllBtn.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent listIntent = new Intent(DetailsActivity.this, ListActivity.class);
                startActivity(listIntent);
            }
        });
    }
}