package in.srishti.musicalapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Call List Activity where the list of songs is displayed
        Intent listIntent = new Intent(MainActivity.this, ListActivity.class);
        MainActivity.this.startActivity(listIntent);
    }
}