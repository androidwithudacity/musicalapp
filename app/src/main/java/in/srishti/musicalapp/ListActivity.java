package in.srishti.musicalapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

public class ListActivity extends AppCompatActivity {

    ArrayList<Song> songsList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        // Displaying description text for the activity in the TextView
        TextView activityDescriptionTextView = findViewById(R.id.songs_list_description_textview);
        String descriptionText = "This screen displays the list of all the songs that can be played.\n" +
                "Tap on a song to play it.";
        activityDescriptionTextView.setText(descriptionText);

        // initializing songs list
        songsList = initializeSongsList();

        // Setting OnClickListener for all songs
        this.setOnClickListenerForSongs();

        // Setting OnClickListener for the buttons in the activity
        Button searchBtn = findViewById(R.id.search_btn);
        searchBtn.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent searchIntent = new Intent(ListActivity.this, SearchActivity.class);
                startActivity(searchIntent);
            }
        } );
    }

    /**
     * Method to bind OnClickListener to all songs
     */
    private void setOnClickListenerForSongs() {
        LinearLayout songsLayout = findViewById(R.id.songs_list_layout); // ViewGroup containing list of all songs

        for (final Song song: songsList) {
            // Dynamically create a TextView on the screen representing a song
            TextView songTextView = (TextView) getLayoutInflater().inflate(R.layout.layout_songs_list, null);
            songTextView.setText(song.getTitle()); // Display song's title
            songsLayout.addView(songTextView); // add created TextView to the songs' ViewGroup in XML

            // bind OnClickListener to the song
            songTextView.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent songIntent = new Intent(ListActivity.this, PlayActivity.class);
                    Bundle songInfoBundle = new Bundle();
                    songInfoBundle.putString(getString(R.string.title), song.getTitle());
                    songInfoBundle.putStringArrayList(getString(R.string.artist), song.getArtists());
                    songIntent.putExtras(songInfoBundle);
                    startActivity(songIntent);
                }
            });
        }
    }

    /**
     * Method to initialize songs list
     * @return list of all songs
     */
    private ArrayList<Song> initializeSongsList() {
        ArrayList<Song> arrList = new ArrayList<>();
        arrList.add(new Song(
                        "Ae Mere Watan Ke Logon",
                        new ArrayList<>(Arrays.asList(new String[] {"Lata Mangeshkar"}))
                )
        );
        arrList.add(new Song(
                        "Aisa Des Hai Mera",
                        new ArrayList<>(Arrays.asList(new String[] {"Lata Mangeshkar", "Udit Narayan", "Gurdas Maan", "Preetha Mazumdar"}))
                )
        );
        arrList.add(new Song(
                        "Kandho Se Milte Hain Kandhe",
                        new ArrayList<>(Arrays.asList(new String[] {"Shankar Mahadevan", "Sonu Nigam", "Hariharan", "Roop Kumar Rathod", "Kunal Ganjawala", "Vijay Prakash"}))
                )
        );
        arrList.add(new Song(
                        "Maa Tujhe Salaam",
                        new ArrayList<>(Arrays.asList(new String[] {"A.R. Rehman"}))
                )
        );
        arrList.add(new Song(
                        "Mera Rang De Basanti Chola",
                        new ArrayList<>(Arrays.asList(new String[] {"Sonu Nigam"}))
                )
        );
        arrList.add(new Song(
                        "Nanha Munna Raahi Hoon",
                        new ArrayList<>(Arrays.asList(new String[] {"Shanti Mathur", "Naushad"}))
                )
        );
        return arrList;
    }
}