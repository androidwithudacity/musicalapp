package in.srishti.musicalapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class PlayActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);

        // Displaying description text for the activity in the TextView
        TextView activityDescriptionTextView = findViewById(R.id.playing_song_description_textview);
        String descriptionText = "This screen displays the title of the song that is currently being played.";
        activityDescriptionTextView.setText(descriptionText);

        // Displaying song title of the song clicked in List Songs activity
        TextView songTextView = findViewById(R.id.playing_song_title);
        String title = getIntent().getExtras().getString(getString(R.string.title));
        songTextView.setText(title);

        // Setting OnClickListener for the buttons in the activity
        Button detailsBtn = findViewById(R.id.details_btn);
        detailsBtn.setOnClickListener(new View.OnClickListener () {
            @Override
            public void onClick(View view) {
                Intent detailsIntent = new Intent(PlayActivity.this, DetailsActivity.class);
                Bundle songInfoBundle = getIntent().getExtras(); // bundling extra information received to pass it on to next activity
                detailsIntent.putExtras(songInfoBundle);
                startActivity(detailsIntent);
            }
        });
    }
}