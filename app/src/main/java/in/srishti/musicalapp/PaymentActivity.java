package in.srishti.musicalapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class PaymentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        // Displaying description text for the activity in the TextView
        TextView activityDescriptionTextView = findViewById(R.id.payment_description_textview);
        String descriptionText = "This screen displays the payment APIs provided by PayUMoney that can be integrated with an app.";
        activityDescriptionTextView.setText(descriptionText);

        // Displaying payment details information in the TextView
        TextView paymentDetailsTextView = findViewById(R.id.payment_details_textview);
        String payumoneyAPIDetails = "PayUmoney provides different APIs for different use cases like "          +
                "payment refund, payment inquiry, check transaction status, create invoices or payment "        +
                "links etc.\n\nIn order to call these APIs, we need to pass authentication in the form of "     +
                "merchant key and authorization header.\n\nValues for both merchant key and Authorization "     +
                "header are available on the PayUmoney dashboard. All API requests are made over "              +
                "HTTPS protocol.";
        paymentDetailsTextView.setText(payumoneyAPIDetails);

        // Setting OnClickListener for the buttons in the activity
        Button homeBtn = findViewById(R.id.home_btn);
        homeBtn.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent listIntent = new Intent(PaymentActivity.this, ListActivity.class);
                startActivity(listIntent);
            }
        });
    }
}